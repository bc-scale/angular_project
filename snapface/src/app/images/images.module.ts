import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageComponent } from './image/image.component';
import { ImagesListComponent } from './images-list/images-list.component';
import { NewImageComponent } from './new-image/new-image.component';
import {SingleImageComponent} from './single-image/single-image.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ImagesRoutingModule } from './images-routing.module';

@NgModule({
  declarations: [
    ImageComponent,
    ImagesListComponent,
    NewImageComponent,
    SingleImageComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ImagesRoutingModule
  ],
  exports: [
    ImageComponent,
    ImagesListComponent,
    NewImageComponent,
    SingleImageComponent
  ]
})
export class ImagesModule { }
