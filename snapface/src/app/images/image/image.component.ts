import { Component, OnInit, Input } from '@angular/core';
import { Image } from '../../core/models/image.model';
import { ImageService } from '../../core/services/image.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit {

  @Input() image!: Image;

  constructor(private imageService: ImageService, private router: Router) { }

  ngOnInit() {}

  onClick() {
    this.imageService.onClick(this.image.id);
  }

  onViewSingleImage() {
    this.router.navigateByUrl('/images/' + this.image.id);
  }

}
