import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ImagesListComponent } from './images-list/images-list.component';
import { SingleImageComponent } from './single-image/single-image.component';
import { NewImageComponent } from './new-image/new-image.component';

import { AuthGuard } from '../core/auth.guard';

const routes: Routes = [

  { path: 'add', component: NewImageComponent, canActivate: [AuthGuard] },
  { path: ':id', component: SingleImageComponent, canActivate: [AuthGuard] },
  { path: '', component: ImagesListComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ImagesRoutingModule { }
